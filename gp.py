from random import random,randint,choice
from copy import deepcopy
from math import log


#Define wrapper used on function nodes
#function, number of parameters, name
class fwrapper:
  def __init__(self,function,childcount,name):
    self.function=function
    self.childcount=childcount
    self.name=name


#Define nodes
#fw=wrapper, children (paramnode or constnode)
#evaluate:  apply wrapper function to children
#display:   visually display the tree
class node:
  def __init__(self,fw,children):
    self.function=fw.function
    self.name=fw.name
    self.children=children
  def evaluate(self,inp):    
    results=[n.evaluate(inp) for n in self.children]
    return self.function(results)
  def display(self,indent=0):
    print ('    '*indent)+self.name
    for c in self.children:
      c.display(indent+1)
    
#Define parameter node
class paramnode:
  def __init__(self,idx):
    self.idx=idx
  def evaluate(self,inp):
    return inp[self.idx]
  def display(self,indent=0):
    print '%sp%d' % ('   '*indent,self.idx)
    
#Define constant node
class constnode:
  def __init__(self,v):
    self.v=v
  def evaluate(self,inp):
    return self.v
  def display(self,indent=0):
    print '%s%d' % ('   '*indent,self.v)

#Define wrapper function
addw=fwrapper(lambda l:l[0]+l[1],2,'add')

subw=fwrapper(lambda l:l[0]-l[1],2,'subtract') 

mulw=fwrapper(lambda l:l[0]*l[1],2,'multiply')

#divw=fwrapper(lambda l:l[0]/l[1],2,'divide')

logw=fwrapper(lambda l:log(l[0]),1,'log')

def divfunc(l):
    if l[1]>0: return l[0]/l[1]
    else: return 0
divw=fwrapper(divfunc,2,'div')

def iffunc(l):
  if l[0]>0: return l[1]
  else: return l[2]
ifw=fwrapper(iffunc,3,'if')

def isgreater(l):
  if l[0]>l[1]: return 1
  else: return 0
gtw=fwrapper(isgreater,2,'isgreater')

def orfunc(l):
    if l[0]>0 or l[1]>0: return 1
    else: return 0
orw=fwrapper(orfunc,2,'or')

def andfunc(l):
    if l[0]>0 and l[1]>0: return 1
    else:return 0
andw=fwrapper(andfunc,2,'and')


#Define function list
#flist=[addw,subw,mulw,ifw,gtw,orw,andw,divw]
flist=[addw,subw,mulw,ifw,gtw,divw]

#Create new random tree
#pc:        total number of parameter
#maxdepth:  max tree depth
#fpr:       probability of creating new node (0.5)
#ppr:       probability of choosing parameter node instead of creating random constant node (0.6)
def makerandomtree(pc,maxdepth=5,fpr=0.5,ppr=0.5):
  if random()<fpr and maxdepth>0:
    f=choice(flist)
    children=[makerandomtree(pc,maxdepth-1,fpr,ppr) 
              for i in range(f.childcount)]
    return node(f,children)
  elif random()<ppr:
    return paramnode(randint(0,pc-1))
  else:
    #return constnode(random())
    return constnode(randint(0,10))

#Create hidden function
def hiddenfunction(x,y,z):
    return x**2+2*y+3*x+5+z

#Create training results for determining fittness
def buildhiddenset():
  rows=[]
  for i in range(200):
    x=randint(0,40)
    y=randint(0,40)
    z=randint(0,40)
    rows.append([x,y,z,hiddenfunction(x,y,z)])
  return rows

#Define score used for evaluate performance
def scorefunction(tree,s):
  dif=0
  for data in s:
    v=tree.evaluate([data[0],data[1],data[2]])
    dif+=abs(v-data[3])
  return dif

#Define Mutation, changing node function, OR replacing node by random function with random children
#Only replacing subtree method is included
#Entire tree is replaced is possible
#No mutation occured is possible
#t:             tree
#pc:            no. of parameter 
#probchange:    probability of replacing subtree (0.1)
def mutate(t,pc,probchange=0.5):
  if random()<probchange:
    return makerandomtree(pc)
  else:
    result=deepcopy(t)
    if hasattr(t,"children"):
      result.children=[mutate(c,pc,probchange) for c in t.children]
    return result

#Define Crossover, combining two tree with randomly choosen subtree
#t1, t2:    tree
#probswap:  probability of crossover, t1 replaced by part of t2
def crossover(t1,t2,probswap=0.5,top=1):
  if random()<probswap and not top:
    return deepcopy(t2) 
  else:
    result=deepcopy(t1)
    if hasattr(t1,'children') and hasattr(t2,'children'):
      result.children=[crossover(c,choice(t2.children),probswap,0) 
                       for c in t1.children]
    return result

#Score ranking
def getrankfunction(dataset):
  def rankfunction(population):
    scores=[(scorefunction(t,dataset),t) for t in population]
    scores.sort()
    return scores
  return rankfunction
  
    
#Main function for genetic programming
#pc:            number of parameter
#popsize:       size of initial population
#rankfunction:  function used for evaluating fittness
#maxgen:        max no. of generation
#mutationrate:  probability of mutation
#breedingrate:  probability of crossover
#pexp:          The rate of decline in probability of selecting lower-ranked program
#pnew:          The probability of introducing new, random program in population
def evolve(pc,popsize,rankfunction,maxgen=5000,pnew=0.1,best=50):
  population=[makerandomtree(pc) for i in range(popsize)]
  for i in range(maxgen):
    scores=rankfunction(population)
    print scores[0][0]
    if scores[0][0]==0: break   #break when fittness fulfiled
    
    # The two best always make it
    newpop=[]
    for j in range(0,best):
        newpop.append(scores[j][1])
    
    # Build the next generation
    while len(newpop)<popsize:
      if random()>pnew:
        newpop.append(mutate(
                      crossover(scores[randint(0,best-1)][1],
                                 scores[randint(0,best-1)][1],
                                probswap=float(randint(1,9))/10),
                        pc,probchange=float(randint(1,9))/10))
      else:
      # Add a random node to mix things up
        newpop.append(makerandomtree(pc,maxdepth=randint(1,10),fpr=float(randint(1,9))/10,ppr=float(randint(1,9))/10))
        
    population=newpop
  scores[0][1].display()    
  return scores[0][1]

#main function    
if __name__ == "__main__":
    #An example used to illustrate a tree structure
    def exampletree():
      return node(ifw,[
                      node(gtw,[paramnode(0),constnode(3)]),
                      node(addw,[paramnode(1),constnode(5)]),
                      node(subw,[paramnode(1),constnode(2)]),
                      ]
                  )


    example=exampletree()
    #x=2, y=3
    example.evaluate([2,3])
    #x=5, y=3
    example.evaluate([5,3])

    example.display()

    #Example
    random1=makerandomtree(2)
    random1.display()
    random1.evaluate([5,3])

    rf=getrankfunction(buildhiddenset())
    #test=evolve(2,500,rf,mutationrate=0.2,breedingrate=0.4,pnew=0.1,best=25)
    #test=evolve(2,5000,rf,mutationrate=0.2,breedingrate=0.4,pnew=0.1,best=50)
    test=evolve(3,2500,rf)