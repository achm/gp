import os as os
import sys as sys

sys.path.append(os.path.abspath(os.path.dirname(sys.argv[0])))
from functionset import *
from evolution import *



class ReadData:
    def __init__(self):
        self.datalist = []
    def ReadFromFile(self,filename):
        with open(filename, "r") as fp:
            while True:
                line=fp.readline()
                if not line : break
                if line[0] == '#': 
                    continue
                else:
                    line_element = line.split(",")
                    line_element = [float(i) for i in line_element]
                    self.datalist.append(line_element)

if __name__ == "__main__":
    #Read data
    data=ReadData()
    data.ReadFromFile("testdata.csv")

    #Get training Data
    rf=Evolution.getrankfunction(data.datalist[1:len(data.datalist)])

    #Get Validation Data
    object=data.datalist[0]

    #Import function set
    flist=FunctionSet()
    flist=[flist.addw()\
          ,flist.subw()\
          ,flist.mulw()\
          ,flist.divw()\
          ,flist.ifw()\
          ,flist.gtw()\
          ,flist.orw()\
          ,flist.andw()\
          ,flist.logw()]

    #Main
    result=Evolution.evolve(4,500,rf,flist,"tesfile12",pnew=0.1,best=10)
