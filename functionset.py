from math import log,exp
from random import random,randint,choice

#Define wrapper used on function nodes
#function, number of parameters, name
class FWrapper:
  def __init__(self,function,childcount,name):
    self.function=function
    self.childcount=childcount
    self.name=name

#Define function set
class FunctionSet:
    @staticmethod
    def addw():
        return FWrapper(lambda l:l[0]+l[1],2,'add')
    @staticmethod
    def subw():
        return FWrapper(lambda l:l[0]-l[1],2,'subtract') 
    @staticmethod
    def mulw():
        return FWrapper(lambda l:l[0]*l[1],2,'multiply')
    @staticmethod
    def divw():
        def divfunc(l):
            if l[1]>0: return l[0]/l[1]
            else: return 1
        return FWrapper(divfunc,2,'div')
    @staticmethod
    def expw():
        return FWrapper(lambda l:exp(l[0]),1,'exp')
    @staticmethod
    def logw():
        def logfunc(l):
            if l[0]>0: return log(l[0])
            else: return 1
        return FWrapper(logfunc,1,'log') 
    @staticmethod
    def ifw():
        def iffunc(l):
          if l[0]>0: return l[1]
          else: return l[2]
        return FWrapper(iffunc,3,'if')
    @staticmethod
    def gtw():
        def isgreater(l):
          if l[0]>l[1]: return 1
          else: return 0
        return FWrapper(isgreater,2,'isgreater')
    @staticmethod
    def orw():
        def orfunc(l):
            if l[0]>0 or l[1]>0: return 1
            else: return 0
        return FWrapper(orfunc,2,'or')
    @staticmethod
    def andw():
        def andfunc(l):
            if l[0]>0 and l[1]>0: return 1
            else:return 0
        return FWrapper(andfunc,2,'and')