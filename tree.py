from random import random,randint,choice

#Define Nodes
#fw=wrapper, children (ParamNode or ConstNode)
#evaluate:  apply wrapper function to children
#display:   visually display the tree
class Node:
    def __init__(self,fw,children):
        self.function=fw.function
        self.name=fw.name
        self.children=children
    def evaluate(self,inp):    
        results=[n.evaluate(inp) for n in self.children]
        return self.function(results)
    def display(self,indent=0):
        print ('    '*indent)+self.name
        for c in self.children:
            c.display(indent+1)
    
#Define parameter Node
class ParamNode:
    def __init__(self,idx):
        self.idx=idx
    def evaluate(self,inp):
        return inp[self.idx]
    def display(self,indent=0):
        print '%sp%d' % ('   '*indent,self.idx)
    
#Define constant Node
class ConstNode:
    def __init__(self,v):
        self.v=v
    def evaluate(self,inp):
        return self.v
    def display(self,indent=0):
        print '%s%d' % ('   '*indent,self.v)

class CreateTree:
    #Create random tree
    #pc:        total number of parameter
    #maxdepth:  max tree depth
    #flist:     list of function set
    #fpr:       probability of creating new Node (0.5)
    #ppr:       probability of choosing parameter Node instead of creating random constant Node (0.5)
    @staticmethod
    def makerandomtree(pc,flist,maxdepth=20,fpr=0.5,ppr=0.5):
        if random()<fpr and maxdepth>0:
            f=choice(flist)
            children=[CreateTree.makerandomtree(pc,flist,maxdepth-1) for i in range(f.childcount)]
            return Node(f,children)
        elif random()<ppr:
            return ParamNode(randint(0,pc-1))
        else:
            return ConstNode(random())
            #return ConstNode(randint(0,10))