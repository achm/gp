from tree import *
from copy import deepcopy
from cloud import cloud
from random import random,randint,choice
import os
import sys
import pickle

class Evolution:
    #Define Mutation, changing node function, OR replacing node by random function with random children
    #Only replacing subtree method is included
    #Entire tree is replaced is possible
    #No mutation occured is possible
    #pc:            no. of parameter 
    #probchange:    probability of replacing subtree
    @staticmethod
    def mutate(t,pc,flist,probchange=0.5):
        if random()<probchange:
            maxdepth=randint(0,10)
            fpr=random()
            ppr=random()
            return CreateTree.makerandomtree(pc,flist,maxdepth)
        else:
            result=deepcopy(t)
            if hasattr(t,"children"):
                result.children=[Evolution.mutate(c,pc,flist) for c in t.children]
            return result

    #Define Crossover, combining two tree with randomly choosen subtree
    #probswap:  probability of crossover, t1 replaced by part of t2
    @staticmethod
    def crossover(t1,t2,top=1,probswap=0.5):
        if random()<probswap and not top:
            return deepcopy(t2) 
        else:
            result=deepcopy(t1)
            if hasattr(t1,'children') and hasattr(t2,'children'):
                result.children=[Evolution.crossover(c,choice(t2.children),0) for c in t1.children]
            return result

    #Score ranking
    @staticmethod
    def getrankfunction(dataset):
        def rankfunction(population):
            scores=[(Evolution.scorefunction(t,dataset),t) for t in population]
            scores.sort()
            return scores
        return rankfunction

    #Define score used for evaluate performance
    @staticmethod
    def scorefunction(tree,s):
        dif=0
        for data in s:
            data_size=len(data)
            v=tree.evaluate(data[0:data_size-1])
            dif+=abs(v-data[data_size-1])
        return dif

    #Main function for genetic programming
    #popsize:       size of initial population
    #rankfunction:  function used for evaluating fittness
    #flist:         function list
    #maxgen:        max no. of generation
    #mutationrate:  probability of mutation
    #breedingrate:  probability of crossover
    #pexp:          The rate of decline in probability of selecting lower-ranked program
    #pnew:          The probability of introducing new, random program in population
    @staticmethod
    def evolve(pc,popsize,rankfunction,flist,savefilename,pnew=0.1,best=50):
        print savefilename
        population=[CreateTree.makerandomtree(pc,flist) for i in range(popsize)]
        filepath=os.path.abspath(os.path.dirname(sys.argv[0])) + "\\model\\"
        try: f=open(filepath+savefilename,'rb')
        except IOError:
            print "file not found, create new file"
        else:
            load_model=pickle.load(f)
            f.close()
            population.append(load_model)
        while True:
            scores=rankfunction(population)
            print scores[0][0]
            f=open(filepath+savefilename,'wb')
            cloud.serialization.cloudpickle.dump(scores[0][1],f)
            f.close()
            if scores[0][0]==0: break   #break when fittness fulfiled
            newpop=[]
            for j in range(0,best):
                newpop.append(scores[j][1])
            # Build the next generation
            while len(newpop)<popsize:
                if random()>pnew:
                    probchange=random()
                    probswap=random()
                    newpop.append(Evolution.mutate(Evolution.crossover(scores[randint(0,best-1)][1],scores[randint(0,best-1)][1],probswap),pc,flist,probchange))
                else:
                    # Add a random node to mix things up
                    maxdepth=randint(0,10)
                    fpr=random()
                    ppr=random()
                    newpop.append(CreateTree.makerandomtree(pc,flist,maxdepth,fpr,ppr))
            population=newpop
        scores[0][1].display()    
        return scores[0][1]